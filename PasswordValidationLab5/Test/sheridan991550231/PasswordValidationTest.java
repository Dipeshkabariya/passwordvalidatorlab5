package sheridan991550231;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class PasswordValidationTest {

	@Test
	public void testIsValidLength( ) {
		assertTrue("Invaild password length",PasswordValidator.isValidLength("12345678"));
		
	}
	@Test
	public void testIsValidLengthException( ) {
		assertFalse("Invaild password length",PasswordValidator.isValidLength( " " ));
		
	}
	@Test
	public void testIsValidLengthExceptionSpaces( ) {
		assertFalse("Invaild password Character",PasswordValidator.isValidLength("  "));
		
	}
	
	@Test
	public void testIsValidLengthBoundaryIn( ) {
		assertTrue("Invaild password length",PasswordValidator.isValidLength( "12345678" ));
		
	}
	
	@Test
	public void testIsValidLengthBoundaryOut( ) {
		assertFalse("Invaild password length",PasswordValidator.isValidLength( "1234567" ));
		
	}
	
	@Test
	public void testHasEnoughDigits() {
		assertTrue("Not Enough Digit",PasswordValidator.hasEnoughDigits("12Dipesh"));
	}
	

	@Test
	public void testHasEnoughDigitsException() {
		assertFalse("Not Enough Digit",PasswordValidator.hasEnoughDigits(" "));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		assertTrue("Not Enough Digit",PasswordValidator.hasEnoughDigits("12Dipesh"));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		assertFalse("Not Enough Digit",PasswordValidator.hasEnoughDigits("1Dipesh"));
	}
	
	@Test 
	public void testHasUpperLowerCaseRegular() {
		
	}
	
	
}
